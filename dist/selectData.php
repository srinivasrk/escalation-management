<?php

// This php scrpt is to retrive data from database !
function createRow($esclid, $empname, $concname, $desc, $status, $timestamp,$product,$cusname)
{

  if($status == 'red')
  {
    $div = '
    <tr class="danger">
      <td class="vert-align" style="width:5%">'.$esclid.'</td>
      <td class="vert-align" style="width:10%">'.$empname.'</td>
      <td class="vert-align" style="width:10%">'.$product.'</td>
      <td class="vert-align" style="width:10%">'.$cusname.'</td>
      <td class="vert-align" style="width:10%">'.$concname.'</td>
      <td class="vert-align" style="width:40%">'.$desc.'</td>
      <td class="vert-align" style="width:8%">Needs Attention </td>
      <td class="vert-align" style="width:10%">'.$timestamp.'</td>
    </tr>';
  }

  else if($status == 'amber')
  {
    $div = '
    <tr class="warning">
   <td class="vert-align" style="width:5%">'.$esclid.'</td>
   <td class="vert-align" style="width:10%">'.$empname.'</td>
   <td class="vert-align" style="width:10%">'.$product.'</td>
   <td class="vert-align" style="width:10%">'.$cusname.'</td>
   <td class="vert-align" style="width:10%">'.$concname.'</td>
   <td class="vert-align" style="width:40%">'.$desc.'</td>
   <td class="vert-align" style="width:8%"> Ongoing Esclation </td>
   <td class="vert-align" style="width:10%">'.$timestamp.'</td>
    </tr>';
  }

  else if($status == 'green')
  {
    $div = '
    <tr class="success">
    <td class="vert-align" style="width:5%">'.$esclid.'</td>
   <td class="vert-align" style="width:10%">'.$empname.'</td>
   <td class="vert-align" style="width:10%">'.$product.'</td>
   <td class="vert-align" style="width:10%">'.$cusname.'</td>
   <td class="vert-align" style="width:10%">'.$concname.'</td>
   <td class="vert-align" style="width:40%">'.$desc.'</td>
    <td class="vert-align" style="width:8%"> Completed </td>
    <td class="vert-align" style="width:10%">'.$timestamp.'</td>
    </tr>';
  }
  return $div;
}
$server= 'localhost';
$username = 'root';
$password ='';
$database = 'emc_m_r';

$conn = mysqli_connect($server, $username, $password,$database);

if(!$conn){
    die("Connection Failed :" . mysqli_connect_error());
}

//$sql = "select week_no,less_50 from metrics where emp_id=1 order by week_no desc";
$sql = "select * from escltbl order by timestamp desc";
$div = "<table class='table' cellpadding='10' border=1 frame=void rules=rows>
    <thead>
      <tr>
        <th style='width:5%'>Escalation ID</th>
        <th style='width:10%'>Created By</th>
        <th style='width:10%'> Product Name </th>
        <th style='width:10%'> Customer Name </th>
        <th style='width:10%'>Concerned Names</th>
        <th style='width:40%'>Description Data</th>
        <th style='width:8%'>Status</th>
        <th style='width:10%'>Last Modified </th>
      </tr>
    </thead>
    <tbody>";
$result = mysqli_query($conn, $sql);
if (mysqli_num_rows($result) > 0) {
  // get the data and put it as json and parse it on client JS
  while ($row = mysqli_fetch_row($result)) {
      $div = $div . createRow($row[0],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7],$row[8]);

   }
echo $div . '  </tbody>
</table>';

} else {
    echo "-1";
}


?>
