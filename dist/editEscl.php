<!DOCTYPE html>
<html>
<head>

  <link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap.min.css"  />
  <link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"  />
  <link rel="stylesheet" href="./custom-css.css"  />
  <script src="./jquery/jquery.min.js" ></script>
  <script src="./bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="./js/editData.js"></script>

</head>
<body>

<!-- This CODE IS FOR NAVIGATION BAR ONLY -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"  target="_blank" href="http://www.emc.com/en-us/index.htm">
        <img alt="Brand" src="./imgs/mylogo.png" style="margin-bottom:20px;">
      </a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="./index.html">Go to Home</a></li>
    </ul>
          <div style="width=100%;float:right;position:absolute;right:0;top:0;padding:20px;">
                
                <p class="text-muted" style="float:right !important;"> Contact srinivas.kulkarni@emc.com for bug report / feature enhancement</p>
                
            </div>
    <p class="navbar-text navbar-center" style="padding-right:20px;">Add Escalation data</p>
        
    </div>

</nav>
<!-- END OF NAVIAGATION -->
<!-- Form to fill the data -->
<div class="container mycontainer">

<div class="form-group">
  <label for="usr">Escalation ID:</label>
  <input type="text" class="form-control disabled" id="esclid" disabled="true">
</div>

<div class="form-group">
  <label for="usr">Created by ID:</label>
  <input type="text" class="form-control disabled" id="createid" disabled="true">
</div>

<div class="form-group">
  <label for="usr">Created by Name:</label>
  <input type="text" class="form-control disabled" id="createname" disabled="true">
</div>
    
<div class="form-group">
  <label for="usr">Product Name :</label>
  <input type="text" class="form-control disabled" id="prodname" disabled="true">
</div>
    
<div class="form-group">
  <label for="usr">Customer Name:</label>
  <input type="text" class="form-control disabled" id="cusname" disabled="true">
</div>

<div class="form-group">
  <label for="pwd">Change Concerned Names:</label>
  <input type="text" class="form-control" id="empname">
</div>
<div class="form-group">
  <label for="pwd">Change Escalation Description:</label>
   <textarea class="form-control" rows="5" id="desc"></textarea>
</div>

<div class="form-group">
  <label for="pwd">Change the status</label>
  <br />
  <label class="radio-inline myprimary"><input type="radio" id="red" name="optradio" value="red" >Needs Attention</label>
  <label class="radio-inline myongoing"><input type="radio" id="amber" name="optradio" value="amber" >Ongoing/secondary Escalation </label>
  <label class="radio-inline mycompleted"><input type="radio" id="green" name="optradio" value="green" >Completed / Handeled Escalation</label>
</div>
<div class="mybtncenter" align="center">
  <button type="button" onclick="submitChanged()" class="btn btn-success">Submit Updated Data</button>
</div>


</div>

<!-- END OF FORM -->

<!-- Footer Details below -->
  
<!-- End of footer -->
</body>
</html>
