<!DOCTYPE html>
<html>
<head>

  <link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap.css"  />
  <link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"  />
  <link rel="stylesheet" href="./custom-css.css"  />
  <script src="./jquery/jquery.min.js" ></script>
  <script src="./bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="./js/addNew.js"></script>

</head>
<body>

<!-- This CODE IS FOR NAVIGATION BAR ONLY -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"  target="_blank" href="http://www.emc.com/en-us/index.htm">
        <img alt="Brand" src="./imgs/mylogo.png" style="margin-bottom:20px;">
      </a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="./index.html">Go to Home</a></li>
    </ul>
    <p class="navbar-text navbar-center" style="padding-right:20px;">Add Escalation data</p>
   <div style="width=100%;float:right;position:absolute;right:0;top:0;padding:20px;">
                
        <p class="text-muted" style="float:right !important;color: #EAE6E6"> Contact srinivas.kulkarni@emc.com for bug report / feature enhancement</p>
                
    </div>
    </div>

</nav>
<!-- END OF NAVIAGATION -->
<!-- Form to fill the data -->
<div class="container mycontainer">


<div class="form-group">
  <label for="usr">Enter your Employee ID:</label>
  <input type="text" class="form-control" id="empid" placeholder="Enter your id / who is submitting the escalation" required="true">
</div>
    
<div class="form-group">
  <label for="usr">Enter your Name:</label>
  <input type="text" class="form-control" id="empname" placeholder="Enter your name / who is submitting the escalation" required="true">
</div>
    
<div class="form-group">
  <label for="usr">Enter the product name:</label>
  <input type="text" class="form-control" id="prodname" placeholder="Enter the product name" required="true">
</div>

<div class="form-group">
  <label for="usr">Enter the customer / account name:</label>
  <input type="text" class="form-control" id="cusname" placeholder="Enter the customer / account name" required="true">
</div>

    
<div class="form-group">
  <label for="pwd">Enter concerned names:</label>
  <input type="text" class="form-control" id="concname" placeholder="SA / IS / PM whoever is invovled" required="true">
</div>
    
<div class="form-group">
  <label for="pwd">Enter the Description:</label>
   <textarea class="form-control" rows="5" id="desc" placeholder="Give a brief overview not more than 1500 characters" required="true"></textarea>
</div>

<div class="form-group">
  <label for="pwd">Select the status</label>
  <br />
  <label class="radio-inline myprimary"><input type="radio" id="red"  name="statradio" value="red">Needs Attention</label>
  <label class="radio-inline myongoing"><input type="radio" id="amber" name="statradio" value="amber">Ongoing/secondary Escalation </label>
  <label class="radio-inline mycompleted"><input type="radio" id="green" name="statradio" value="green">Completed / Handeled Escalation</label>
</div>
<div class="mybtncenter" align="center">
  <button id="submit_data" onclick="submitData()" type="button" class="btn btn-success">Submit Data</button>
</div>


</div>

<!-- END OF FORM -->

<!-- Footer Details below -->
  
<!-- End of footer -->
</body>
</html>
