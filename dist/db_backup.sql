-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.24 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for emc_m_r
CREATE DATABASE IF NOT EXISTS `emc_m_r` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `emc_m_r`;


-- Dumping structure for table emc_m_r.escltbl
CREATE TABLE IF NOT EXISTS `escltbl` (
  `escID` int(11) NOT NULL AUTO_INCREMENT,
  `empID` int(10) NOT NULL DEFAULT '0',
  `empName` varchar(500) NOT NULL DEFAULT '0',
  `concerned` varchar(500) DEFAULT NULL,
  `escDesc` varchar(5000) NOT NULL DEFAULT '0',
  `status` varchar(100) NOT NULL DEFAULT '0',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`escID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table emc_m_r.escltbl: ~5 rows (approximately)
/*!40000 ALTER TABLE `escltbl` DISABLE KEYS */;
REPLACE INTO `escltbl` (`escID`, `empID`, `empName`, `concerned`, `escDesc`, `status`, `timestamp`) VALUES
	(11, 158404, 'Srinivas R Kulkarni', 'Srinivas / Hassan', 'Project ID : 12345 , Cause : , To DO !', 'green', '2017-02-26 20:01:55'),
	(12, 158404, 'srinivas', 'Srinivas R Kulkarni', 'No Escalation', 'green', '2017-02-26 23:44:51'),
	(13, 158404, 'srinivas', 'Srinivas R Kulkarni', 'No Escalation - 1', 'green', '2017-02-26 23:46:20'),
	(14, 158404, 'woot', 'woot woot', 'Test', 'amber', '2017-02-26 23:58:10'),
	(15, 158404, 'Srinivas R Kulkarni', 'Mr. XYZ , Mr. ABC etc ', 'esÂ·caÂ·laÂ·tion\nËŒeskÉ™ËˆlÄSH(É™)\nnoun\na rapid increase; a rise.\ncost escalations\nsynonyms:	increase, rise, hike, growth, leap, upsurge, upturn, climb More\nan increase in the intensity or seriousness of something; an intensification.\nan escalation of violence', 'amber', '2017-02-27 13:01:24');
/*!40000 ALTER TABLE `escltbl` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
