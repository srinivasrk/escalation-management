$(document).ready(function(){

  var a = window.location.toString();

  var name = a.substring(a.indexOf("=")+1);

  if(a == name)
  {

    var esclid = prompt("Enter your Escalation ID number : ");
    var empid = prompt("Enter your Employee ID : ");

    if(esclid.trim() == '' || empid.trim() == ''){
      alert("Invalid inputs please try again ! If error persists contact administrator ! Redirecting to Homepage");
      document.location.href="/dist/";
    }

    else{

      $.ajax({
        type:'POST',
        url: "checkeditData.php",
        data: { esclId : esclid, empId : empid },
        dataType: "html",
        cache: false,
        async: false,
        success: function(data){
          if(data != '-1'){
              //sleep(5000);

                console.log(data);
                var JsonObject= JSON.parse(data);
                document.getElementById('esclid').value = JsonObject[0]['escID'];
                document.getElementById('empname').value = JsonObject[0]['concerned'];
                document.getElementById('desc').value = JsonObject[0]['escDesc'];
                document.getElementById('createid').value = JsonObject[0]['empID'];
                document.getElementById('createname').value = JsonObject[0]['empName'];
                document.getElementById('cusname').value = JsonObject[0]['cusName'];
                document.getElementById('prodname').value = JsonObject[0]['product'];
              
                var status = JsonObject[0]['status'];

                if(status.trim() == 'red'){
                  document.getElementById('red').checked = "true";
                }
                else if(status.trim() == 'amber'){
                  document.getElementById('amber').checked = "true";
                }
                else {
                  document.getElementById('green').checked = "true";
                }
               //$("tbody").html(data);
              // Set the inner HTML
              //drawChart(array, "My Chart", "Data");
          }
          else{
            alert("Invalid Data ! If problem persists please contact administrator");
            document.location.href="/esclmgmt/dist/";
          }

      },
       async: "false",
      });
    }
  }
  else{
    //this is passed from admin page
      
    $.ajax({
      type:'POST',
      url: "checkeditData.php",
      data: { esclId : name },
      dataType: "html",
      cache: false,
      async: false,
      success: function(data){
        if(data != '-1'){
            //sleep(5000);
              console.log(data);
              var JsonObject= JSON.parse(data);
              document.getElementById('esclid').value = JsonObject[0]['escID'];
              document.getElementById('empname').value = JsonObject[0]['concerned'];
              document.getElementById('desc').value = JsonObject[0]['escDesc'];
              document.getElementById('createid').value = JsonObject[0]['empID'];
              document.getElementById('createname').value = JsonObject[0]['empName'];

              var status = JsonObject[0]['status'];

              if(status.trim() == 'red'){
                document.getElementById('red').checked = "true";
              }
              else if(status.trim() == 'amber'){
                document.getElementById('amber').checked = "true";
              }
              else {
                document.getElementById('green').checked = "true";
              }
             //$("tbody").html(data);
            // Set the inner HTML
            //drawChart(array, "My Chart", "Data");
        }
        else{
          alert("Unable to load data from server please contact the administrator !");
          document.location.href="/esclmgmt/dist/";
        }

    },
     async: "false",
    });

  }
  });

function submitChanged(){
  var esclid = document.getElementById('esclid').value;
  var concname = document.getElementById('empname').value;
  var desc = document.getElementById('desc').value;

  if(document.getElementById('red').checked){
    var status = document.getElementById('red').value;
  }
  else if(document.getElementById('amber').checked){
    var status = document.getElementById('amber').value;
  }
  else if(document.getElementById('green').checked){
    var status = document.getElementById('green').value;
  }
  else{
    alert("Please enter the status before submitting !");
    return;
  }
  if (esclid.trim() == '' || concname.trim() == '' || desc.trim() == '' || status.trim() == '')
  {
    alert("Please fill all the details");
  }
  else{
    if (confirm('Are you sure you want to submit the data ?')) {
    // Save it!
      $.ajax({
        type:'POST',
        url: "addData.php",
        data: { esclId : esclid ,  concName : concname, descData : desc, statusVal : status },
        dataType: "html",
        cache: false,
        async: false,
        success: function(data){
          if(data != '-1'){
              //sleep(5000);
                alert("Successfully Udated to Database !");
                document.location.href = "./index.html"
               //$("tbody").html(data);
              // Set the inner HTML
              //drawChart(array, "My Chart", "Data");
          }
          else{
            alert("Unable to edit database. Please contact administrator");
            document.location.href = "./index.html"
          }

      },
       async: "false",
      });

    } else {

    }
  }
}
