<!DOCTYPE html>
<html>
<head>

  <link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap.min.css"  />
  <link rel="stylesheet" href="./bootstrap-3.3.7-dist/css/bootstrap-theme.min.css"  />
  <link rel="stylesheet" href="./custom-css.css"  />
  <script src="./jquery/jquery.min.js" ></script>
  <script src="./bootstrap-3.3.7-dist/js/bootstrap.min.js" ></script>
  <script src="./js/dashboard.js"></script>


</head>
<body>

<!-- This CODE IS FOR NAVIGATION BAR ONLY -->
  <nav class="navbar navbar-inverse">
    <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"  target="_blank" href="http://www.emc.com/en-us/index.htm">
        <img alt="Brand" src="./imgs/mylogo.png" style="margin-bottom:20px;">
      </a>
    </div>

    <ul class="nav navbar-nav">
      <li><a href="./index.html">Go to Home</a></li>
    </ul>
    <p class="navbar-text navbar-center" style="padding-right:20px;">Dashboard</p>

          <div style="width=100%;float:right;position:absolute;right:0;top:0;padding:20px;">
                
                    <p class="text-muted" style="float:right !important;color: #EAE6E6"> Contact srinivas.kulkarni@emc.com for bug report / feature enhancement</p>
                
            </div>
        
    </div>

</nav>
<!-- END OF NAVIAGATION -->

<!-- Begin the Buttons -->
<!-- <div class="container">
  <div class="row" style="align-contentcenter;">
    <div class="col-lg-4 myalerts">
      <button class="btn btn-primary myprimary"  type="button">
        Primary Escalations <span class="badge">4</span>
      </button>
    </div>
    <div class="col-lg-4 myalerts">
      <button class="btn btn-primary myongoing" type="button">
        On-going Escalations <span class="badge">4</span>
      </button>
    </div>
    <div class="col-lg-4 myalerts ">
      <button class="btn btn-primary mycompleted" type="button">
        Completed Escalations <span class="badge">4</span>
      </button>
    </div>
  </div>
</div>
 End of buttons -->
<!-- Begin of rows -->
<div id="filter" class="container" style="margin-top40px;">

    <label for="input"> Enter the Escalation ID to edit : </label> <input type="text" id="esclID" class="form-control" style="width : 100px; display:inline;" />
    <button id="submit_data" style="display : inline; margin-left:20px;" onclick="editEsclAdmin()" type="button" class="btn btn-success">Edit Data</button>


</div>
<div id="rowdetails" class="" style="margin-top:40px;">
<hr  />
<hr  />
  <!-- <table class="table">
      <thead>
        <tr>
          <th>Escalation ID</th>
          <th>Employee Name</th>
          <th>Description Data</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>

        <tr class="success">
          <td class="vert-align">1</td>
          <td class="vert-align">EMP NAME</td>
          <td class="vert-align">Description</td>
          <td class="vert-align">
              Completed
          </td>
        </tr>
        <tr class="danger">
          <td class="vert-align">2</td>
          <td class="vert-align">EMP NAME</td>
          <td class="vert-align">Description</td>
          <td class="vert-align">
              Needs Attention
          </td>
        </tr>

        <tr class="warning">
          <td class="vert-align">3</td>
          <td class="vert-align">EMP NAME</td>
          <td class="vert-align">Since we do not have any page to link it to, and we do not want to get a "404" message, we put # as the link. In real life it should of course been a real URL to the "Search" page.</td>
          <td>
             Ongoing
          </td>
        </tr>

      </tbody>
    </table>

END OF Rows -->
</div>
<!-- Footer Details below -->
  
<!-- End of footer -->
</body>
</html>
